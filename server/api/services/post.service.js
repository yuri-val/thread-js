import postRepository from '../../data/repositories/post.repository';
import postReactionRepository from '../../data/repositories/post-reaction.repository';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const create = (userId, post) => postRepository.create({
    ...post,
    userId
});

export const update = (userId, post) => postRepository.getPostById(post.id)
    .then(postRecord => postRecord.update({
        ...post,
        userId
    }));

export const setReaction = async (userId, { postId, isLike = true }) => {
    // define the callback for future use as a promise
    const updateOrDelete = react => (react.isLike === isLike
        ? postReactionRepository.deleteById(react.id)
        : postReactionRepository.updateById(react.id, { isLike }));

    const reaction = await postReactionRepository.getPostReaction(userId, postId);

    let reactDiff = {};
    if (!reaction) reactDiff = { like: isLike ? 1 : 0, dislike: !isLike ? 1 : 0 };
    else if (reaction.isLike) reactDiff = { like: -1, dislike: !isLike ? 1 : 0 };
    else reactDiff = { like: isLike ? 1 : 0, dislike: -1 };

    if (reaction) await updateOrDelete(reaction);
    else await postReactionRepository.create({ userId, postId, isLike });

    // the result is an integer when an entity is deleted
    return reactDiff;
};
