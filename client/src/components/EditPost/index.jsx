import React from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Icon, Image, Segment, Modal } from 'semantic-ui-react';

import styles from './styles.module.scss';

class EditPost extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            post: props.post,
            isUploading: false
        };
    }

    handleEditPost = async () => {
        await this.props.editPost(this.state.post);
        this.props.close();
    }

    handleUploadFile = async ({ target }) => {
        this.setState({ isUploading: true });
        try {
            const image = await this.props.uploadImage(target.files[0]);
            this.setState(state => ({
                ...state,
                isUploading: false,
                post: {
                    ...state.post,
                    image,
                    imageId: image.id
                }
            }));
        } catch {
            // TODO: show error
            this.setState({ isUploading: false });
        }
    }

    handleBodyChange = (ev) => {
        const body = ev.target.value;
        this.setState(state => ({
            ...state,
            post: {
                ...state.post,
                body
            }
        }));
    }

    render() {
        const { image, body } = this.state.post;
        const { isUploading } = this.state;
        const { close } = this.props;
        const imageLink = image ? image.link : '';

        return (
            <Modal open onClose={close}>
                <Modal.Header className={styles.header}>
                    <span>Edit Post</span>
                </Modal.Header>
                <Modal.Content>
                    <Segment>
                        <Form onSubmit={this.handleEditPost}>
                            <Form.TextArea
                                name="body"
                                value={body}
                                placeholder="What is the news?"
                                rows="8"
                                onChange={this.handleBodyChange}
                            />
                            {imageLink && (
                                <div className={styles.imageWrapper}>
                                    <Image className={styles.image} src={imageLink} alt="post" />
                                </div>
                            )}
                            <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
                                <Icon name="image" />
                                Attach image
                                <input name="image" type="file" onChange={this.handleUploadFile} hidden />
                            </Button>
                            <Button floated="right" color="blue" type="submit">Save</Button>
                        </Form>
                    </Segment>
                </Modal.Content>
            </Modal>
        );
    }
}

EditPost.propTypes = {
    editPost: PropTypes.func.isRequired,
    uploadImage: PropTypes.func.isRequired,
    post: PropTypes.shape({
        id: PropTypes.string.isRequired
    }).isRequired,
    close: PropTypes.func.isRequired
};

export default EditPost;
